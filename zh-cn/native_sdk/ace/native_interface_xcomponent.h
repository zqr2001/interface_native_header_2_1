/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OH_NativeXComponent Native XComponent
 * @{
 *
 * @brief 描述ArkUI XComponent持有的surface和触摸事件，该事件可用于EGL/OpenGLES和媒体数据输入，并显示在ArkUI XComponent上。
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file native_interface_xcomponent.h
 *
 * @brief 声明用于访问Native XComponent的API。
 *
 * @since 8
 * @version 1.0
 */

#ifndef _NATIVE_INTERFACE_XCOMPONENT_H_
#define _NATIVE_INTERFACE_XCOMPONENT_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 枚举API访问状态。
 *
 * @since 8
 * @version 1.0
 */
enum {
    /** 成功结果。 */
    OH_NATIVEXCOMPONENT_RESULT_SUCCESS = 0,
    /** 失败结果。 */
    OH_NATIVEXCOMPONENT_RESULT_FAILED = -1,
    /** 无效参数。 */
    OH_NATIVEXCOMPONENT_RESULT_BAD_PARAMETER = -2,
};

enum OH_NativeXComponent_TouchEventType {
    /** 手指按下时触发触摸事件。 */
    OH_NATIVEXCOMPONENT_DOWN = 0,
    /** 手指抬起时触发触摸事件。 */
    OH_NATIVEXCOMPONENT_UP,
    /** 手指按下状态下在屏幕上移动时触发触摸事件。 */
    OH_NATIVEXCOMPONENT_MOVE,
    /** 触摸事件取消时触发事件。 */
    OH_NATIVEXCOMPONENT_CANCEL,
    /** 无效的触摸类型。 */
    OH_NATIVEXCOMPONENT_UNKNOWN,
};

#define OH_NATIVE_XCOMPONENT_OBJ ("__NATIVE_XCOMPONENT_OBJ__")
const uint32_t OH_XCOMPONENT_ID_LEN_MAX = 128;
const uint32_t OH_MAX_TOUCH_POINTS_NUMBER = 10;

struct OH_NativeXComponent_TouchPoint {
    /** 手指的唯一标识符。 */
    int32_t id = 0;
    /** 触摸点相对于屏幕左边缘的x坐标。 */
    float screenX = 0.0;
    /** 触摸点相对于屏幕上边缘的y坐标。 */
    float screenY = 0.0;
    /** 触摸点相对于XComponent组件左边缘的x坐标。 */
    float x = 0.0;
    /** 触摸点相对于XComponent组件上边缘的y坐标。 */
    float y = 0.0;
    /** 触摸事件的触摸类型。 */
    OH_NativeXComponent_TouchEventType type = OH_NativeXComponent_TouchEventType::OH_NATIVEXCOMPONENT_UNKNOWN;
    /** 指垫和屏幕之间的接触面积。 */
    double size = 0.0;
    /** 当前触摸事件的压力。 */
    float force = 0.0;
    /** 当前触摸事件的时间戳。 */
    long long timeStamp = 0;
    /** 当前点是否被按下。 */
    bool isPressed = false;
};

/**
 * @brief 代表接触点信息。
 *
 * @since 8
 * @version 1.0
 */
struct OH_NativeXComponent_TouchEvent {
    /** 手指的唯一标识符。 */
    int32_t id = 0;
    /** 触摸点相对于屏幕左边缘的x坐标。 */
    float screenX = 0.0;
    /** 触摸点相对于屏幕上边缘的y坐标。 */
    float screenY = 0.0;
    /** 触摸点相对于XComponent组件左边缘的x坐标。 */
    float x = 0.0;
    /** 触摸点相对于XComponent组件上边缘的y坐标。 */
    float y = 0.0;
    /** 触摸事件的触摸类型。 */
    OH_NativeXComponent_TouchEventType type = OH_NativeXComponent_TouchEventType::OH_NATIVEXCOMPONENT_UNKNOWN;
    /** 指垫和屏幕之间的接触面积。 */
    double size = 0.0;
    /** 当前触摸事件的压力。 */
    float force = 0.0;
    /** 产生当前触摸事件的设备的ID。 */
    int64_t deviceId = 0;
    /** 当前触摸事件的时间戳。 */
    long long timeStamp = 0;
    /** 当前触摸点的数组。 */
    OH_NativeXComponent_TouchPoint touchPoints[OH_MAX_TOUCH_POINTS_NUMBER];
    /** 当前接触点的数量。 */
    uint32_t numPoints = 0;
};

/**
 * @brief 提供封装的OH_NativeXComponent实例。
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_NativeXComponent OH_NativeXComponent;

/**
 * @brief 注册surface生命周期和触摸事件回调。
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_NativeXComponent_Callback {
    /** 创建surface时调用。 */
    void (*OnSurfaceCreated)(OH_NativeXComponent* component, void* window);
    /** 当surface改变时调用。 */
    void (*OnSurfaceChanged)(OH_NativeXComponent* component, void* window);
    /** 当surface被破坏时调用。 */
    void (*OnSurfaceDestroyed)(OH_NativeXComponent* component, void* window);
    /** 当触摸事件被触发时调用。 */
    void (*DispatchTouchEvent)(OH_NativeXComponent* component, void* window);
} OH_NativeXComponent_Callback;

/**
 * @brief 获取ArkUI XComponent的id。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param id 指示用于保存此OH_NativeXComponent实例的ID的字符缓冲区。
 *        请注意，空终止符将附加到字符缓冲区，因此字符缓冲区的大小应至少比真实id长度大一个单位。
 *        建议字符缓冲区的大小为[OH_XCOMPONENT_ID_LEN_MAX + 1]。
 * @param size 指示指向id长度的指针。
 * @return 返回执行的状态代码。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetXComponentId(OH_NativeXComponent* component, char* id, uint64_t* size);

/**
 * @brief 获取ArkUI XComponent持有的surface的大小。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param window 表示NativeWindow句柄。
 * @param width 指示指向当前surface宽度的指针。
 * @param height 指示指向当前surface高度的指针。
 * @return 返回执行的状态代码。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetXComponentSize(
    OH_NativeXComponent* component, const void* window, uint64_t* width, uint64_t* height);

/**
 * @brief 获取ArkUI XComponent组件相对屏幕左上顶点的偏移量。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param window 表示NativeWindow句柄。
 * @param x 指示指向当前surface的x坐标的指针。
 * @param y 指示指向当前surface的y坐标的指针。
 * @return 返回执行的状态代码。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetXComponentOffset(
    OH_NativeXComponent* component, const void* window, double* x, double* y);

/**
 * @brief 获取ArkUI XComponent调度的触摸事件。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param window 表示NativeWindow句柄。
 * @param touchEvent 指示指向当前触摸事件的指针。
 * @return 返回执行的状态代码。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeXComponent_GetTouchEvent(
    OH_NativeXComponent* component, const void* window, OH_NativeXComponent_TouchEvent* touchEvent);

/**
 * @brief 为此OH_NativeXComponent实例注册回调。
 *
 * @param component 表示指向OH_NativeXComponent实例的指针。
 * @param callback 指示指向surface生命周期和触摸事件回调的指针。
 * @return 返回执行的状态代码。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeXComponent_RegisterCallback(OH_NativeXComponent* component, OH_NativeXComponent_Callback* callback);

#ifdef __cplusplus
};
#endif
#endif // _NATIVE_INTERFACE_XCOMPONENT_H_
